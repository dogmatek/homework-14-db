package ru.sber.jd;

import ru.sber.jd.entities.StockValue;
import ru.sber.jd.utils.DbUtils;

import java.sql.Connection;
import java.sql.SQLException;

public class Application {

    private static Connection sa;

    public static void main(String[] args) throws SQLException {
        Connection connection = DbUtils.createConnection();


        DbUtils.createTable(connection);    // создаем таблицу
        DbUtils.deleteAll(connection);      // Удаляем все строки таблицы


        DbUtils.insertValue(connection, StockValue.builder()
                .stock("SBER")
                .date("2020-10-20")
                .open(203.0)
                .high(209.76)
                .low(202.11)
                .close(209.54)
                .vol(80919230)
                .build()
        );

        DbUtils.insertValue(connection, StockValue.builder()
                .stock("DSKY")
                .date("2020-10-20")
                .open(113.86)
                .high(117.98)
                .low(112.82)
                .close(115.94)
                .vol(1774738)
                .build()
        );

        DbUtils.insertValue(connection, StockValue.builder()
                .stock("GAZP")
                .date("2020-10-20")
                .open(163.74)
                .high(167.75)
                .low(161.89)
                .close(166.53)
                .vol(50479186)
                .build()
        );
        DbUtils.insertValue(connection, StockValue.builder()
                .stock("SBER")
                .date("2020-10-19")
                .open(192.34)
                .high(195.5)
                .low(191.1)
                .close(194.43)
                .vol(13809886)
                .build()
        );

        //System.out.println(String.format("Найдены котирвки = %s", DbUtils.selectAll(connection)));
        String ticket = "SBER";
        System.out.println(String.format("Найдены котирвки по тикету %s: %s", ticket, DbUtils.selectTicket(connection,ticket)));

    }



}
