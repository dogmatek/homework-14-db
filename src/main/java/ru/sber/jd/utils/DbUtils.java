package ru.sber.jd.utils;


import ru.sber.jd.entities.StockValue;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbUtils {

    public static Connection createConnection()throws SQLException {
        return DriverManager.getConnection("jdbc:h2:~/testomework-14-bd",
                "sa", "");
    }

    public static void createTable(Connection connection) throws SQLException {
        Statement statement =connection.createStatement();
        statement.execute("CREATE TABLE IF NOT EXISTS stock_value(stock text, date DATE, open FLOAT, " +
                "high FLOAT, low FLOAT, close FLOAT, vol INT)");
        connection.commit();
    }

    public static List<StockValue> selectAll(Connection connection) throws SQLException {
        Statement statement =connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM stock_value");

        List<StockValue> stockValues = new ArrayList<>();
        while (resultSet.next()){
            String stock = resultSet.getString(1);
            String date = resultSet.getString(2);
            Double open = resultSet.getDouble(3);
            Double high = resultSet.getDouble(4);
            Double low = resultSet.getDouble(5);
            Double close = resultSet.getDouble(6);
            Integer vol = resultSet.getInt(7);
            stockValues.add(StockValue.builder()
                    .stock(stock)
                    .date(date)
                    .open(open)
                    .high(high)
                    .low(low)
                    .close(close)
                    .vol(vol)
                    .build());

        }
        return stockValues;
    }

    public static List<StockValue> selectTicket(Connection connection, String ticket) throws SQLException {
        Statement statement =connection.createStatement();
        ResultSet resultSet = statement.executeQuery(String.format("SELECT * FROM stock_value WHERE stock ='%s'", ticket));

        List<StockValue> stockValues = new ArrayList<>();
        while (resultSet.next()){
            String stock = resultSet.getString(1);
            String date = resultSet.getString(2);
            Double open = resultSet.getDouble(3);
            Double high = resultSet.getDouble(4);
            Double low = resultSet.getDouble(5);
            Double close = resultSet.getDouble(6);
            Integer vol = resultSet.getInt(7);
            stockValues.add(StockValue.builder()
                    .stock(stock)
                    .date(date)
                    .open(open)
                    .high(high)
                    .low(low)
                    .close(close)
                    .vol(vol)
                    .build());

        }
        return stockValues;
    }


    public static void insertValue(Connection connection, StockValue stockValue) throws SQLException {
        Statement statement =connection.createStatement();

        statement.execute(String.format("INSERT INTO stock_value (stock, date, open, high, low, close, vol)" +
                "VALUES ('%s','%s','%s',%s,%s,%s,%s)", stockValue.getStock(), stockValue.getDate(),
                stockValue.getOpen(), stockValue.getHigh(), stockValue.getLow(),
                stockValue.getClose(), stockValue.getVol()));
        connection.commit();
    }

    public static void deleteAll(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("DELETE FROM stock_value");
        connection.commit();
    }
}
