package ru.sber.jd.entities;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class StockValue {
    private String stock;
    private String date;
    private Double open;
    private Double high;
    private Double low;
    private Double close;
    private Integer vol;
}
